#include "uart.h"
#include "bme280.h"
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include "pid.h"
#include "pwm.h"
#include "status.h"
#include "signal.h"
#include "interface.h"

FILE *logger, *curve_file;


void *temperature_routine(void *arg) {
    get_room_temperature_routine();
    return NULL;
}

void *handle_command_routine(void *arg) {
    float ti;

    while(1) {
        do
        {
            ti = get_user_command();
            usleep(200000);
        } while (ti <= 0);
    }

    return NULL;
}

void *main_routine(void *arg) {
    pid_configura_constantes(30, 0.2, 400);
    turn_on_device();
    send_system_status(1);
    start_process();
    send_functioning_status(1);
    logger = fopen("logger.csv", "w+");
    fprintf(logger, "Time, Temperatura Interna, Temperatura Externa, Temperatura de Referencia, PID\n");

    while (1) {
        if (!(get_device_status() == get_process_status() && get_process_status() == 1)) {
            turn_off(FAN);
            turn_off(RESISTOR);
            continue;
        }

        double reference;
        do {
            reference = get_reference_temperature();
        } while (reference == -1.0);
        usleep(500000);
        double internal;
        do {
            internal = get_temperature(INTERNAL);
        } while(internal == -1.0);
        usleep(500000);
        pid_atualiza_referencia(reference);
        double controle = pid_controle(internal);
        
        if (controle > 0) {
            while (turn_off(FAN));
            while (control(RESISTOR, (int)controle) != 0);
        }
        if (controle < 0) {
            while (turn_off(RESISTOR));
            controle = controle > -40 ? -40 : controle;
            while (control(FAN, (-1) * (int)controle) != 0);
        }
        float room_temperature = get_external_temperature();
        send_room_temperature(room_temperature);
        send_signal((int) controle);

        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        char time_string[50];
        sprintf(time_string, "%02d/%02d/%d %02d:%02d:%02d", tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
        fprintf(logger, "%s, %.2lf, %.2f, %.2lf, %.2lf\n", time_string, internal, room_temperature, reference, controle);

        usleep(1000000);
    }
    return NULL;
}

void *read_file(void *arg) {
    char buffer[20];
    curve_file = fopen("curva_reflow.csv", "r");
    int current_time = 0, last_time = 0;
    int current_temperature = 0;
    int count = 0;

    while(fgets(buffer, 200, curve_file) != NULL)
    {
        count++;
        if (count == 1) continue;
        current_time = atoi(strtok(buffer, ","));
        current_temperature = atoi(strdup(strtok(NULL, ",")));
        sleep(current_time - last_time);
        send_reference(current_temperature);
        last_time = current_time;
    }

    fclose(curve_file);
    return NULL;
}

void shutdown() {
    while(turn_off(FAN));
    while(turn_off(RESISTOR));
    fclose(logger);
    exit(0);
}

int main() {
    signal(SIGINT, shutdown);
    time_t rawtime;
    time(&rawtime);

    pthread_t temp_thread, command_thread, pid_thread, csv_thread;
    pthread_create(&temp_thread, NULL, temperature_routine, NULL);
    pthread_create(&command_thread, NULL, handle_command_routine, NULL);
    pthread_create(&pid_thread, NULL, main_routine, NULL);
    pthread_create(&csv_thread, NULL, read_file, NULL);
    menu();    
}
