#include "status.h"

short process_status = 0;
short device_status = 0;

void turn_on_device() {
    device_status = 1;
}

void turn_off_device() {
    device_status = 0;
}
void start_process() {
    process_status = 1;
}
void stop_process() {
    process_status = 0;
}

short get_device_status() {
    return device_status;
}
short get_process_status() {
    return process_status;
}