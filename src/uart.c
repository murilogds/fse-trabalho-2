#include "uart.h"

short mode = 1;
float reference_temperature;

int open_uart() {
    return open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);
}

void set_attributes(int uart_filestream) {
    struct termios options;
    tcgetattr(uart_filestream, &options);
    options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart_filestream, TCIFLUSH);
    tcsetattr(uart_filestream, TCSANOW, &options);
}

void get_info(int uart_filestream, unsigned char sub_code) {
    unsigned char tx_buffer[13] = {DEVICE, GET_CODE, sub_code, 5, 6, 0, 1};
    int crc_size = 7;

    short crc = get_CRC(tx_buffer, crc_size);
    
    memcpy(&tx_buffer[crc_size], &crc, 2);

    write(uart_filestream, &tx_buffer[0], crc_size+2);
}

int get_data_size(unsigned char sub_code)
{
    switch (sub_code)
    {
        case SEND_REFERENCE:
        case SEND_SIGNAL:
        case SEND_ROOM_TEMP:
            return 4;
            break;
        case SEND_SYSTEM_STATUS:
        case SET_CONTROL_MODE:
        case SEND_FUNCTIONING_STATUS:
        default:
            return 1;
            break;
    }
}

void send_info(int uart_filestream, unsigned char sub_code, unsigned char *data) {
    unsigned char tx_buffer[13] = {DEVICE, SEND_CODE, sub_code, 5, 6, 0, 1};
    int crc_size = 7 + get_data_size(sub_code);
    memcpy(&tx_buffer[7], data, get_data_size(sub_code));

    short crc = get_CRC(tx_buffer, crc_size);
    memcpy(&tx_buffer[crc_size], &crc, 2);
    write(uart_filestream, &tx_buffer[0], crc_size+2);
}

void read_response(int uart_filestream, unsigned char* rx_buffer) {
    int rx_length = read(uart_filestream, (void*)rx_buffer, 10);
    rx_buffer[rx_length] = '\0';
}

float get_temperature(unsigned char sub_code) {
    int uart_filestream = -1;
    unsigned char rx_buffer[9];
    float temperature;
    short crc, check_crc;

    uart_filestream = open_uart();
    if (uart_filestream == -1) {
        return -1.0;
    }

    set_attributes(uart_filestream);
    get_info(uart_filestream, sub_code);
    usleep(100000);
    read_response(uart_filestream, &rx_buffer[0]);

    memcpy(&temperature, &rx_buffer[3], 4);
    memcpy(&crc, &rx_buffer[7], 2);
    check_crc = get_CRC(&rx_buffer[0], 7);

    if (rx_buffer[0] != 0x00 || rx_buffer[1] != 0x23 || rx_buffer[2] != sub_code || crc != check_crc)
        return -1.0;
    close(uart_filestream);

    return temperature;
}

int get_user_command() {
    int uart_filestream = -1;
    unsigned char rx_buffer[9];
    int command;
    short crc, check_crc;

    uart_filestream = open_uart();
    if (uart_filestream == -1) {
        return -1;
    }

    set_attributes(uart_filestream);
    get_info(uart_filestream, USER_COMMAND);
    usleep(100000);
    read_response(uart_filestream, &rx_buffer[0]);

    memcpy(&command, &rx_buffer[3], 4);
    memcpy(&crc, &rx_buffer[7], 2);
    check_crc = get_CRC(&rx_buffer[0], 7);

    if (rx_buffer[0] != 0x00 || rx_buffer[1] != 0x23 || rx_buffer[2] != USER_COMMAND || crc != check_crc)
        return -1;
    close(uart_filestream);

    switch (command) {
        case 0xA1:
            turn_on_device();
            send_system_status(1);
            break;
        case 0xA2:
            turn_off_device();
            send_system_status(0);
            break;
        case 0xA3:
            start_process();
            send_functioning_status(1);
            break;
        case 0xA4:
            stop_process();
            send_functioning_status(0);
            break;
        case 0xA5:
            mode = (mode + 1)%2;
            set_control_mode(mode);
            break;
        default:
            break;
    }

    return command;
}

int send_signal(int control_signal) {
    int uart_filestream = -1;

    uart_filestream = open_uart();
    if (uart_filestream == -1) {
        return -1;
    }

    unsigned char buffer[4];
    memcpy(&buffer[0], &control_signal, 4);

    set_attributes(uart_filestream);
    send_info(uart_filestream, SEND_SIGNAL, buffer);
    close(uart_filestream);

    return 0;
}

int send_reference(float reference) {
    reference_temperature = reference;
    int uart_filestream = -1;

    uart_filestream = open_uart();
    if (uart_filestream == -1) {
        return -1;
    }

    unsigned char buffer[4];
    memcpy(&buffer[0], &reference, 4);

    set_attributes(uart_filestream);
    send_info(uart_filestream, SEND_REFERENCE, buffer);
    close(uart_filestream);

    return 0;
}

int send_system_status(short status) {
    int uart_filestream = -1;

    uart_filestream = open_uart();
    if (uart_filestream == -1) {
        return -1;
    }

    unsigned char buffer[1];
    memcpy(&buffer[0], &status, 1);

    set_attributes(uart_filestream);
    send_info(uart_filestream, SEND_SYSTEM_STATUS, buffer);
    close(uart_filestream);

    return 0;
}

int set_control_mode(short control_mode) {
    int uart_filestream = -1;

    uart_filestream = open_uart();
    if (uart_filestream == -1) {
        return -1;
    }

    unsigned char buffer[1];
    memcpy(&buffer[0], &control_mode, 1);

    set_attributes(uart_filestream);
    send_info(uart_filestream, SET_CONTROL_MODE, buffer);
    close(uart_filestream);

    return 0;
}

int send_functioning_status(short status) {
    int uart_filestream = -1;

    uart_filestream = open_uart();
    if (uart_filestream == -1) {
        return -1;
    }

    unsigned char buffer[1];
    memcpy(&buffer[0], &status, 1);

    set_attributes(uart_filestream);
    send_info(uart_filestream, SEND_FUNCTIONING_STATUS, buffer);
    close(uart_filestream);

    return 0;
}

int send_room_temperature(float temp) {
    int uart_filestream = -1;

    uart_filestream = open_uart();
    if (uart_filestream == -1) {
        return -1;
    }

    unsigned char buffer[4];
    memcpy(&buffer[0], &temp, 4);

    set_attributes(uart_filestream);
    send_info(uart_filestream, SEND_ROOM_TEMP, buffer);
    close(uart_filestream);

    return 0;
}

float get_reference_temperature() {
    if (mode == 1 && reference_temperature > 0) return reference_temperature;
    else return reference_temperature = get_temperature(REFERENCE);
}