#include "interface.h"

void read_constants() {
    double kp, ki, kd;
    printf("Kp: ");
    scanf("%lf", &kp);
    printf("Ki: ");
    scanf("%lf", &ki);
    printf("Kd: ");
    scanf("%lf", &kd);
    pid_configura_constantes(kp, ki, kd);
}

void read_reference_temperature() {
    float temp;
    printf("Digite a temperatura de referência: ");
    scanf("%f", &temp);
    send_reference(temp);
}

void menu() {
    set_control_mode(1);
    while (1) {
        system("clear");
        int opt = 0;
        printf("CONTROLE DE FORNO\n");
        printf("------------------\n");
        printf("1 - Atualizar constantes (Kp, Ki e Kd)\n");
        printf("2 - Enviar temperatura de referência manualmente\n");
        printf("Digite o número do comando desejado: ");
        scanf("%d", &opt);
        switch (opt) {
            case 1:
                read_constants();
                break;
            case 2:
                read_reference_temperature();
                break;
            default:
                break;
        }
    }
}