# Projeto 2 - FSE
## Objetivo
A ideia desse trabalho é ter um sistema de controle da temperatura de um forno de solda, usando a resistência e a ventoinha do mesmo para se obter as temperaturas desejadas. Nesse projeto foram utilizados conceitos como:

* Circuitos de potência PWM (Controle da resistência e ventoinha);
* Comunicação UART via MODBUS (Para manter os dados atualizados com a ESP32);
* Leitura da temperatura interna usando o driver bme280 da BOSCH.

## Execução
### Passo 1 - Compilar
Para compilar, basta executar o seguinte comando no terminal (dentro da rasp):
```bash
$ make
```

### Passo 2 - Rodar
Para compilar, basta executar o seguinte comando no terminal após a compilação:
```bash
$ make run
```

## Demonstração
Aqui se encontram gráficos do comportamento do sistema seguindo o arquivo de curva "curva_reflow.csv"

### Temperaturas

![Gráfico de Temperatura](img/temperatue.png)
**Obs**: As temperaturas foram medidas no forno 4, que não tem o bme280 funcional, por isso a temperatura externa igual a 0.

### Sinal de Controle

![Gráfico de Controle](img/control.png)



