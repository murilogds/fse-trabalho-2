#ifndef STATUS_H
#define STATUS_H

void turn_on_device();
void turn_off_device();
void start_process();
void stop_process();

short get_device_status();
short get_process_status();

#endif