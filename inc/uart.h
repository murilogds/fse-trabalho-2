#ifndef UART_H
#define UART_H
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <stdlib.h>
#include <string.h>
#include "crc.h"
#include "status.h"

#define DEVICE 0x01

#define GET_CODE 0x23
#define SEND_CODE 0x16

#define INTERNAL 0xC1
#define REFERENCE 0xC2
#define USER_COMMAND 0xC3
#define SWITCH_STATUS 0xC3
#define SEND_SIGNAL 0xD1
#define SEND_REFERENCE 0xD2
#define SEND_SYSTEM_STATUS 0xD3
#define SET_CONTROL_MODE 0xD4
#define SEND_FUNCTIONING_STATUS 0xD5
#define SEND_ROOM_TEMP 0xD6

// GET INFO
float get_temperature(unsigned char sub_code);
float get_reference_temperature();
int get_user_command();

// SEND INFO
int send_signal(int signal);
int send_reference(float reference);
int send_system_status(short status);
int set_control_mode(short mode);
int send_functioning_status(short status);
int send_room_temperature(float temp);

#endif